namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(maxLength: 100),
                        RoleDisplayName = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Birthday = c.DateTime(),
                        Phone = c.String(maxLength: 15),
                        Email = c.String(maxLength: 100),
                        UserName = c.String(maxLength: 100),
                        UserPassword = c.String(maxLength: 100),
                        GuiD = c.Guid(nullable: false),
                        UserRoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Roles", t => t.UserRoleID, cascadeDelete: true)
                .Index(t => t.UserRoleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UserRoleID", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "UserRoleID" });
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
        }
    }
}
