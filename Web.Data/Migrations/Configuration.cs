namespace Web.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Web.Model.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Data.WebDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Web.Data.WebDbContext context)
        {
            context.Roles.AddOrUpdate(x => x.ID,
            new Role() { ID = 1, RoleName = "admin", RoleDisplayName = "Administrator" },
            new Role() { ID = 2, RoleName = "editor", RoleDisplayName = "Editor" }
            );

            context.Users.AddOrUpdate(x => x.ID,
            new User() { ID = 1, Name = "Jane Austen", Birthday = null, Email = "", Phone = "", GuiD = Guid.NewGuid(), UserName = "jane", UserPassword = "123", UserRoleID = 1 },
            new User() { ID = 2, Name = "Join Tom", Birthday = null, Email = "", Phone = "", GuiD = Guid.NewGuid(), UserName = "join", UserPassword = "123", UserRoleID = 2 }
            );

        }
    }
}
