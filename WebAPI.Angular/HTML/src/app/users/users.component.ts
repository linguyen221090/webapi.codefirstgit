import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { User } from './shared/user.model';
import { UserService } from './shared/user.service';
import { RoleService } from '../roles/shared/role.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers:[UserService, RoleService]
})
export class UsersComponent implements OnInit {

  nameSubmit = "";

  formUser = "Form User";

  inforUser = "Information User";

  boolSelect: boolean = true;

  roleName = "";
  
  errorUserRole = false;

  token = localStorage.getItem('userToken');

  constructor(private userService: UserService, private roleService: RoleService) { }

  ngOnInit() {
    this.userService.getUsers();
    this.roleService.getRoles();
    this.resetForm();
  }

  showForEdit(user: User) {
    this.userService.user = Object.assign({}, user);
    this.nameSubmit = "<i class=\"fa fa-edit\"></i> Edit";
  }

  setUserRoleID(id: any, form: NgForm): void {
    
    }

    testForm(form: NgForm)
    {
      if(form.value.userroleid == -1)
      {
        console.log(form.value.userroleid);
        return form.invalid;
      }
    }

    onSubmit(form: NgForm) {
      if(form.value.userroleid == -1)
      {
        return this.errorUserRole = true;
      }
      if (form.value.ID == -1) {
        this.userService.postUser(form.value).toPromise().then(() => {
          this.resetForm(form);
          this.userService.getUsers();
        });
      }
      else {
        this.userService.putUser(form.value.ID, form.value).toPromise().then(() => {
          this.resetForm(form);
          this.userService.getUsers();
        });
      }
    }

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
      this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
      this.errorUserRole = false;
      this.userService.user = {
        ID: -1,
        Name: '',
        Birthday: new Date(2000, 12, 12),
        Phone: '',
        Email: '',
        UserName: '',
        UserPassword: '',
        UserRoleID: -1
      }
  }

  onDelete(id: number, form?: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.userService.deleteUser(id).toPromise().then(() => {
        this.userService.getUsers();
        this.resetForm(form);
      });
    }
  }

  get diagnostic() { return JSON.stringify(this.userService.user); }

}
