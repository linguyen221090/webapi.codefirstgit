export class User {
    ID: number;
    Name: string;
    Birthday: Date;
    Phone: string;
    Email: string;
    UserName: string;
    UserPassword: string;
    UserRoleID: number;
}
