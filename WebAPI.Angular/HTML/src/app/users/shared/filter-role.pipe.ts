import { Pipe, PipeTransform } from '@angular/core';
import { Role } from '../../roles/shared/role.model';

@Pipe({
  name: 'test',
  pure: false
})

export class FilterPipe implements PipeTransform {
  transform(items: any[], role: Role): any[] {

    if (!items || !role) {
        return items;
    }

    return items.filter(item  => item.RoleName.equals(role.RoleName)); 
   }
}