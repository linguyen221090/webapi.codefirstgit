import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { LoginService } from './shared/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private router : Router) { }

  ngOnInit() {
    this.loginService.user = {
      ID: -1,
      Name: '',
      Birthday: new Date(2000, 12, 12),
      Phone: '',
      Email: '',
      UserName: '',
      UserPassword: '',
      UserRoleID: -1
    }
  }

  onSubmit(form: NgForm) {
    if(form.invalid)
    {
      console.log("error not valid");
      return;
    }
    this.loginService.userAuthentication(this.loginService.user.UserName ,this.loginService.user.UserPassword).subscribe((data : any)=>{
      localStorage.setItem('userToken' , data.access_token);
      this.router.navigate(['/user']);
      console.log("ok");
    },
    (err : HttpErrorResponse)=>{
      console.log("error");
    });
  }

  get diagnostic() { return JSON.stringify(this.loginService.user); }

}
