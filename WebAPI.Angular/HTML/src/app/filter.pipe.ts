import { Pipe, PipeTransform } from '@angular/core';
import { Role } from './roles/shared/role.model';

@Pipe({
  name: 'filter',
  pure: false
})

export class FilterPipe implements PipeTransform {
  transform(items: any[], id: number): any[] {

    if (!items || !id) {
        return items;
    }

    return items.filter(item  => item.ID == id); 
   }
}