import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Role } from './shared/role.model';
import { RoleService } from './shared/role.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css'],
  providers:[RoleService]
})
export class RolesComponent implements OnInit {


  formRole = "Form Role";

  inforRole = "Information Role";

  nameSubmit = "Add";

  username;

  phusername;

  searchText;

  token = localStorage.getItem("userToken");

  // searchRole: Role = {
  //   ID: 3,
  //   RoleName: "user",
  //   RoleDisplayName: "User"
  // };

  searchRoles: Role[] = null;

  characters = [
    'Finn the human',
    'Jake the dog',
    'Princess bubblegum',
    'Lumpy Space Princess',
    'Beemo1',
    'Beemo2'
  ]

  errorRoleName = "";

  constructor(private roleService : RoleService) { }

  ngOnInit() {
    this.roleService.getRoles();
    this.phusername = "Ladies and Gentlemen!";
    this.resetForm();
  }

  // transform(items: any[], filter: Role ): any {  
  //   if (!items || !filter) {  
  //       return items;  
  //   }  
  //   return items.filter(item => item.name.indexOf(filter.RoleDisplayName) !== -1);  
  // }  

  resetForm(form?: NgForm) {
    if(form != null)
      form.reset();

    this.roleService.role = {
      ID: -1,
      RoleName: "",
      RoleDisplayName: ""
    }
    this.nameSubmit = "<i class=\"fa fa-plus-square\"></i> Add";
  }

  onKey(event: any) { // without type info
    this.roleService.getRolesByName(this.searchText);
  }

  onSubmit(form: NgForm) {
    if(form.invalid)
    {
      this.errorRoleName = "Please, enter to all input!";
      return form.errors;
    }
    if(this.token == null || this.token == "")
    {
      this.errorRoleName = "Please, enter to all input!";
      return form.errors;
    }
    if (form.value.ID == -1) {
      this.roleService.postRole(form.value, this.token).toPromise().then(() => {
        this.resetForm(form);
        this.roleService.getRoles();
      })
      .catch(e => console.error('An error occurred', e));;
    }
    else {
      this.roleService.putRole(form.value, this.token).toPromise().then(() => {
        this.resetForm(form);
        this.roleService.getRoles();
      })
      .catch(e => console.error('An error occurred', e));;
    }
  }

  showForEdit(role: Role) {
    this.roleService.role = Object.assign({}, role);
    this.nameSubmit = "<i class=\"fa fa-edit\"></i> Edit";
  }

  onDelete(id: number, form?: NgForm) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.roleService.deleteRole(id, this.token)
      .toPromise()
      .then(() => {
        this.roleService.getRoles();
        this.resetForm(form);
      })
      .catch(e => console.error('An error occurred', e));
    }
  }

  get diagnostic() { return JSON.stringify(this.roleService.role); }

}
