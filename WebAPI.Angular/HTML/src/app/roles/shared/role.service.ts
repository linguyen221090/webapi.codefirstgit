import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions, RequestMethod, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Role } from './role.model';

@Injectable()
export class RoleService {

  role: Role;
  roles: Role[];

  constructor(private http: Http) { }

  getRoles(){
    this.http.get('http://localhost:53655/api/role')
    .map((data : Response) =>{
      return data.json() as Role[];
    })
    .toPromise()
    .then(x => {
      this.roles = x;
    })
    .catch(e => console.error('An error occurred', e));
  }

  getRolesByID(id){
    this.http.get('http://localhost:53655/api/role/' + id)
    .map((data : Response) =>{
      return data.json() as Role;
    }).toPromise().then(x => {
      this.role = x;
    })
  }

  getRolesByName(name){
    this.http.get('http://localhost:53655/api/role/search/' + name)
    .map((data : Response) =>{
      return data.json() as Role[];
    }).toPromise().then(x => {
      this.roles = x;
    })
  }

  putRole(role, token: String) {
    var body = JSON.stringify(role);
    var headerOptions = new Headers({'Content-Type':'application/json', 'Authorization': 'bearer ' + token});
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:53655/api/role/',
      body,
      requestOptions)
  }

  postRole(role: Role, token: String)
  {
    var body = JSON.stringify(role);
    var headerOptions = new Headers({'Content-Type':'application/json', 'Authorization': 'bearer ' + token});
    var requestOptions = new RequestOptions({method:RequestMethod.Post,headers:headerOptions});
    return this.http.post('http://localhost:53655/api/role',body,requestOptions);
  }

  deleteRole(id: Number, token: String)
  {
    var headerOptions = new Headers({'Content-Type':'application/json', 'Authorization': 'bearer ' + token});
    var requestOptions = new RequestOptions({headers:headerOptions});
    return this.http.delete('http://localhost:53655/api/role/' + id, requestOptions);
  }

}
