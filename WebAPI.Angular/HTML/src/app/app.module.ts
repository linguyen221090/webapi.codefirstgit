import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes, ChildrenOutletContexts } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { FilterPipe } from './filter.pipe';
import { AppComponent } from './app.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { LoginService } from './login/shared/login.service';
import { AuthGuard } from '../app/auth/auth-guard.service';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'role', component: RolesComponent, canActivate: [AuthGuard] },
  { path: 'user', component: UsersComponent, canActivate: [AuthGuard] },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    FilterPipe,
    AppComponent,
    RolesComponent,
    PageNotFoundComponent,
    UsersComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    )
  ],
  providers: [LoginService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
