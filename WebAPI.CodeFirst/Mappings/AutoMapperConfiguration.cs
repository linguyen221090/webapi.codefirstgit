﻿using AutoMapper;
using Web.Model.Models;
using WebAPI.CodeFirst.Models;

namespace WebAPI.CodeFirst.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<User, UserViewModels>();
            });
        }

    }
}