﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.CodeFirst.Models;
using Web.Model.Models;
using Web.Service;
using AutoMapper;
using System.Text;
using System.Net.Http.Headers;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace WebAPI.CodeFirst.Controllers
{
    public class UserController : ApiController
    {
        private IUserService _userService;
        private IRoleSerice _roleService;

        public UserController(IUserService userService, IRoleSerice roleService)
        {
            this._userService = userService;
            this._roleService = roleService;
        }

        [HttpGet]
        public IEnumerable<UserViewModels> GetAllUser()
        {
            var users = _userService.GetAll();
            if (users == null)
            {
                return null;
            }
            var viewModels = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModels>>(users);

            return viewModels;
        }

        [ResponseType(typeof(User))]
        [HttpGet]
        [Route("{id:int}")]
        public IHttpActionResult GetUserByID(int id)
        {
            var user = _userService.GetByID(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        [HttpPost]
        public IHttpActionResult CreateUser(User user)
        {
            if (ModelState.IsValid)
            {
                user.GuiD = Guid.NewGuid();
                _userService.Add(user);
                _userService.Save();
            }
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult UpdateUser(User user)
        {
            if (ModelState.IsValid)
            {
                _userService.Update(user);
                _userService.Save();
            }
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteUser(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid user id");
            var user = _userService.GetByID(id);
            if(user == null)
                return BadRequest("Not found User");
            _userService.Delete(user);
            _userService.Save();
            return Ok();
        }

        [Route("~/api/testuser")]
        public HttpResponseMessage Get()
        {
            // Get a list of products from a database.
            var users = _userService.GetAll();
            if (users == null)
            {
                return null;
            }
            var viewModels = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModels>>(users);

            // Write the list to the response body.
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, viewModels);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };
            return response;
        }
    }
}