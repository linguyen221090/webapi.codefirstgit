﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.CodeFirst.Models
{
    public class UserViewModels
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public Guid GuiD { get; set; }
        public int UserRoleID { get; set; }
    }
}